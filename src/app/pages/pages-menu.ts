import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'صفحه اصلی',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'امکانات',
    group: true,
  },
  {
    title: 'لیست کافه ها',
    icon: 'nb-keypad',
    link: '/pages/cafe',
  },
  {
    title: 'نقشه',
    icon: 'nb-location',
    link: '/pages/maps/gmaps',
  },
  {
    title: 'ثبت نام / ورود',
    icon: 'nb-locked',
    children: [
      {
        title: 'ورود',
        link: '/auth/login',
      },
      {
        title: 'ثبت نام',
        link: '/auth/register',
      },
      {
        title: 'بازیابی پسورد',
        link: '/auth/reset-password',
      },
    ],
  },
  {
      title: 'اعلانات',
      icon: 'nb-notifications',
      link: '/pages/notification',
  },
];
