import { Component, OnInit } from '@angular/core';
import {CafeService} from "../cafe.service";
import {NbSearchService} from "@nebular/theme";

@Component({
  selector: 'ngx-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  tops: any;
  bests: any;
  searchResults: any;

  constructor(private cafeService: CafeService,
    private searchService: NbSearchService
  ) {
      this.searchService.onSearchSubmit()
          .subscribe((data: any) => {
              this.cafeService.cafeSearch(data.term).subscribe(result => {
                  this.searchResults = result;
              });
          })
  }


  ngOnInit() {
      this.cafeService.getMain().subscribe(data => {
          this.tops = data.top;
      });

      this.cafeService.getMain().subscribe(data => {
          this.bests = data.best;
      });
  }

}
