import {Component, OnInit, ViewChild} from '@angular/core';
import {NbSearchService} from "@nebular/theme";
import {CafeService} from "../../../cafe.service";

@Component({
  selector: 'ngx-gmaps',
  templateUrl: './gmaps.component.html',
  styleUrls: ['./gmaps.component.scss'],
})


export class GmapsComponent implements OnInit {
    bests: any;
    @ViewChild('AgmMap') agmMap: AgmMap;

    constructor(private cafeService: CafeService) {
    }

    ngOnInit() {
        this.cafeService.getMain().subscribe(data => {
            this.bests = data.best;
        });

        this.agmMap.mapReady.subscribe(map => {
            this.cafeService.getMain().subscribe(data => {
                const bounds = new google.maps.LatLngBounds();
                for (let cafe of data.best) {
                    bounds.extend(new google.maps.LatLng(cafe.latitude, cafe.longitude));
                }
                map.fitBounds(bounds);
            });
        }
    }

    lat = 51.678418;
    lng = 7.809007;
}
