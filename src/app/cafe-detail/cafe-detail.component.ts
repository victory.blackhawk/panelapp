import { Component, OnInit } from '@angular/core';
import {CafeService} from "../cafe.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'ngx-cafe-detail',
  templateUrl: './cafe-detail.component.html',
  styleUrls: ['./cafe-detail.component.scss']
})
export class CafeDetailComponent implements OnInit {
  cafe: any = {};

  constructor(private cafeService: CafeService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
      this.route.params.subscribe(params => {
          const id = params['id'];
          if (id) {
              this.cafeService.getDetail(id).subscribe((cafe: any) => {
                  this.cafe = cafe;
              });
          }
      });
  }

}
