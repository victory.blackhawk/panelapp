import { Component, OnInit } from '@angular/core';
import {CafeService} from "../cafe.service";

@Component({
  selector: 'ngx-cafe',
  templateUrl: './cafe.component.html',
  styleUrls: ['./cafe.component.scss']
})
export class CafeComponent implements OnInit {
    tops : any;

  constructor(private cafeService: CafeService) { }

  ngOnInit() {
      this.cafeService.getMain().subscribe(data => {
          this.tops = data.top;
      });
  }

}
