import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Rx";
import {HttpClient} from "@angular/common/http";

@Injectable({
    providedIn: 'root'
})
export class CafeService {

    constructor(private http: HttpClient) { }
    getMain(): Observable<any> {
        return this.http.get('http://cafe.re2.ir/api/v1/main_data');
    }

    getDetail(id: string) {
        return this.http.get('http://cafe.re2.ir/api/v1/cafe_data/'+id);
    }

    cafeSearch(query: string) {
        return this.http.get('http://cafe.re2.ir/api/v1/search?q='+query);
    }

}
